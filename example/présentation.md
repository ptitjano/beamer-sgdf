---
title: Ceci est un exemple de titre sur plusieurs lignes
date: mars 2021
lang: fr
aspectratio: 169
---

# Engagements chiffrés 2020-2025

Réduction des émissions carbone de *l’association* de  **21,5%**.

# liste des branches SGDF

- farfadets
- louveteaux - jeannettes
- scouts - guides
- pionniers
- compagnons

# Diapositive avec deux colonnes

::: columns

:::: column
texte à gauche
::::

:::: column

texte à droite
::::

:::
texte en dessous des deux colonnes

# Affichage d'une image avec légende

![Un coeur bat](./example/coeur-bat.jpeg){width=4cm}

# Affichage d'une autre image sans légende

\centerimage[width=2cm]{./example/coeur-bat.jpeg}

Texte en dessous en de l'image
