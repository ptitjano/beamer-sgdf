# Présentation

Le fichier [sgdf.tex](sgdf.tex) définit quelques éléments `beamer` pour avoir le rendu le plus proche possible des [masques PowerPoint SGDF officiels](https://www.sgdf.fr/vos-ressources/doc-en-stock/category/144-presentation-et-visibilite-du-mouvement?download=2410:modele-masque-ppt).

Les éléments suivants sont définis :

- utilisation des polices d'écriture `Raleway` et `Sarabun` ;
- utilisation de la couleur officielle ;
- page de titre (`setbeamertemplate{title page}`) avec `title`, `date` et `institute` ;
- titre de diapositive avec logo SGDF (`setbeamertemplate{frametitle}`) ;
- pied de page avec les pictogrammes SGDF et le numéro de diapositive (`setbeamertemplate{footline}`) ;
- listes avec un pictogramme de tente (`setbeamertemplate{itemize items}`) ;
- pas de numéros de figure pour les légendes d'images (`setbeamertemplate{caption}`) ;
- ajout de la commande LaTeX `centerimage` pour centrer une image sans légende.

L'utilisation de `xelatex` est recommandée afin de pouvoir facilement incorporer les polices d'écritures `Raleway` et `Sarabun` sans devoir les [convertir au format type1](https://tex.stackexchange.com/questions/315678/making-ttf-font-latex-ready).

# Utilisation avec pandoc

Le logiciel [pandoc](https://pandoc.org/) permet notamment de générer des présentation au format pdf à partir d'un fichier mardown. Il suffit d'utiliser l'option `--include-in-header` pour utiliser les personnalisations définies dans [sgdf.tex](sgdf.tex).

```bash
pandoc example/présentation.md --pdf-engine=xelatex -t beamer --include-in-header=sgdf.tex -o example/présentation.pdf
```

# Exemple

Cette [présentation](example/présentation.pdf) est obtenue avec `pandoc` à partir ce [fichier markdown](example/présentation.md).

# Références

- [Pandoc manual](https://pandoc.org/MANUAL.html)
- [BEAMER appearance cheat sheet](http://www.cpt.univ-mrs.fr/~masson/latex/Beamer-appearance-cheat-sheet.pdf)
- [utilisation des beamercolorbox](http://mcclinews.free.fr/latex/beamermodif/beamerbox.html)
